<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <form action="/kirim" method="POST">
        @csrf
        <h1>Buat Account Baru</h1>
        <h3>Sign Up Form</h3>
        
    <label for="firstname">First name</label><br><br>
        <input type="text" id="firstname" name="namadepan"><br><br>
        <label for="lastname">Last name</label><br><br>
        <input type="text" id="lastname" name="namabelakang"><br><br>

    <label>Gender:</label><br><br>
        <input type="radio" name="gn" value="Male">Male<br>
        <input type="radio" name="gn" value="Female">Female<br>
        <input type="radio" name="gn" value="Other">Other <br><br>
        
    <label>Nationality:</label><br><br>
        <select name="nation">
            <option value="indo">Indonesian</option>
            <option value="usa">Amerika</option>
            <option value="eng">Inggris</option>
        </select><br><br>

    <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bindo">Bahasa Indonesia <br>
        <input type="checkbox" name="eng">English <br>
        <input type="checkbox" name="oth">Other <br>
        <br>

    <label>Bio:</label><br><br>
        <textarea id="bio_1" name="bio" rows="12" cols="32"></textarea><br> 
    
        <input type="submit" value="Sign Up">

    </form>
    
</body>
</html>